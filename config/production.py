# coding=utf-8
# Copyright (c) 2018      On-Core
# Author: zlq
# Date: 2018-05-01

"""
线上环境的配置模块
"""

# database setting
database = {
    "mongodb": {
        "name": "hbdb",
        "username": "",
        "password": "",
        "host": "127.0.0.1",
        "port": 27017,
    },
    "mysql": {
        "name": "hbdb",
        "username": "",
        "password": "",
        "host": "127.0.0.1",
        "port": 3306,
    }
}


# tornado setting
tornado = {
    "debug": False,
    "cookie_secret": "cookie_secret_code",
}