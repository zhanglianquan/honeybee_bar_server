# coding=utf-8
# Copyright (c) 2018      On-Core
# Author: zlq
# Date: 2018-05-01

"""
开发测试环境的配置模块
"""

# database setting
database = {
    "mongodb": {
        "name": "test_db",
        "username": "",
        "password": "",
        "host": "127.0.0.1",
        "port": 27017,
    }
}


# tornado setting
tornado = {
    "debug": True,
    "autoreload": True,
    "cookie_secret": "cookie_secret_code",
}
