# coding=utf-8
# Copyright (c) 2018      On-Core
# Author: zlq
# Date: 2018-05-01


"""
根据不同的环境的加载不同的配置模块
"""

import os
import sys
import src.utils as util

env = os.environ.get('env', "test")


@util.singleton
class LoadConfigManager(object):
    """
    加载环境配置的单例
    """

    def load_config(self):
        if env.lower() == "production":
            # 线上配置
            module_name = "config.production"
        else:
            # 测试配置
            module_name = "config.development"

        __import__(module_name)
        return sys.modules[module_name]


