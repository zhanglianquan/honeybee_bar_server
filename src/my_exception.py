# coding=utf-8
# Copyright (c) 2018      On-Core
# Author: zlq
# Date: 2018-03-18

"""
    自定义的异常模块
"""


class MyException(Exception):
    """
    自定义的异常的基类
    """
    message = 'An unknown exception'

    def __init__(self, msg=None, **kwargs):
        self.kwargs = kwargs
        if msg is None:
            msg = self.message

        try:
            msg = msg % kwargs
        except Exception:
            msg = self.message
        self.message = msg
        super(MyException, self).__init__(msg)


class CustomServerInternalError(Exception):
    """
    自定义的服务器内部错误
    """
    message = u'自定义的服务器内部错误'

    def __init__(self, message=None):
        if message is not None:
            self.message = message

        super(CustomServerInternalError, self).__init__(self.message)


class AXMRouterPathOverapError(MyException):
    message = u'RequestHandler子类建立的路由映射覆盖了之前的' \
              u'router_path=%(router_path)s 类名=%(class_name)s 的' \
              u' 路由路径,这样会导致请求调用不到旧的业务,' \
              u'建议重新设置新的路由路径 '


class JSONDataFormatError(MyException):
    message = u'传递的JSON数据格式有误,请检查是否是正确的JSON格式!'



