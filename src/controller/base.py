# coding=utf-8
# Copyright (c) 2018      On-Core
# Author: zlq
# Date: 2018-03-18

"""
 所有请求的处理基类
"""

import tornado.web
import os
import re
import src.utils as util
import tornado.escape as escape
import src.my_exception as exp

# 收集到的路由映射表
handlers = []
re_replace = re.compile(r"(['/','\','//','\\'])")


# 收集 controller文件夹下,所有的路由映射处理函数
def collect_handlers():
    folder_path = os.path.dirname(__file__)
    filters = ['.py']
    return_file_list = []
    util.filter_files_efficient(folder_path, filters, return_file_list)
    if return_file_list:
        root_project_path = os.path.dirname(os.path.dirname(
            os.path.dirname(__file__)))
        for file_name in return_file_list:
            repl_str = file_name.replace(root_project_path, "")
            module_name = re_replace.sub('.', repl_str[1:][:-3])
            __import__(module_name)
    return handlers


# 收集路由映射表的装饰器
def router(router_path):
    """
    :param router_path : 映射的路由路径, 是Restful API 规范
    """
    def handle_router(cls):
        module_name = cls.__module__ + "." + cls.__name__
        router_handler = (router_path, module_name)

        def path_overlap_error(router_tuple):
            if router_tuple[0] == router_path:
                raise exp.AXMRouterPathOverapError(router_path=router_path,
                                                   class_name=router_tuple[1])

        if router_handler not in handlers:
            map(path_overlap_error, handlers)
            handlers.append(router_handler)

        return cls
    return handle_router


# 所有tornado的tornado.web.RequestHandler子类返回模板
def rest_command_template(func):
    def _rest_command(*args, **kwargs):
        respose = args[0]
        try:
            func(*args, **kwargs)
            if respose._write_buffer:
                return

            msg = {"result": "success", "message": "ok"}
            return respose.write(msg)
        except exp.CustomServerinternalError as e:
            status_code = 500
            reason = e.message
        except tornado.web.MissingArgumentError as e:
            status_code = 400
            reason = e.message if e.message \
                else "tornado.web.MissingArgumentError"
        except Exception as e:
            status_code = 500
            reason = e.message
        msg = {"result": "failed",
               "message": reason}
        respose.set_status(status_code=status_code)
        return respose.write(msg)
    return _rest_command


class BaseHandler(tornado.web.RequestHandler):
    """
    所有的tornado.web.RequestHandler子类的基类
    """
    def prepare(self):
        origin = self.request.headers.get("Origin",
                                          "http://localhost:8080")
        self.set_header('Access-Control-Allow-Origin', origin)
        self.set_header('Access-Control-Allow-Methods',
                        'GET, POST, PUT, HEAD, DELETE, OPTIONS')
        self.set_header('Access-Control-Allow-Headers', '*')
        self.set_header('Access-Control-Allow-Credentials', 'true')
        self.request_json_data = {}
        try:
            self.request_json_data = escape.json_decode(self.request.body) \
                if self.request.body else {}
        except ValueError as e:
            self.send_error(status_code=400, error_message=
            exp.JSONDataFormatError().message)

    def write_error(self, status_code, **kwargs):
        if 'error_message' in kwargs:
            msg = {"result": "failed",
                   "message": kwargs['error_message']}
            self.write(msg)

    def write_succcess(self):
        self.write({"result": "success", "message": "ok"})

    def post(self, *args, **kwargs):
        return self.write("base post")
        self.set_status(200)

    def get(self, *args, **kwargs):
        self.write("base get")
        self.set_status(200)

    def put(self, *args, **kwargs):
        self.write("base put")
        self.set_status(200)

    def options(self, *args, **kwargs):
        self.write("base options")
        self.set_status(200)






