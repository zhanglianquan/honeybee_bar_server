# coding=utf-8
# Copyright (c) 2018      On-Core
# Author: zlq
# Date: 2018-05-01

""" 用户处理模块"""
import src.controller.base as base


@base.router(r"^/users(/(.*))?")
class UserHandler(base.BaseHandler):
    def get(self, *args, **kwargs):
        self.write("hello user get")

    def post(self, *args, **kwargs):
        self.write("hello user get")
